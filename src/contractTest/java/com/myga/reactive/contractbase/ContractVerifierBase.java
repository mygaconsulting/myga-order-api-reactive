package com.myga.reactive.contractbase;

import com.myga.reactive.ContractTestApplication;
import com.myga.reactive.domain.order.Order;
import com.myga.reactive.domain.order.OrderService;
import com.myga.reactive.domain.order.OrderStatus;
import com.myga.reactive.domain.order.menuitem.ItemType;
import com.myga.reactive.domain.order.menuitem.MenuItem;
import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = ContractTestApplication.class)
@ActiveProfiles({"contract", "test"})
public class ContractVerifierBase {

    @Value("${local.server.port:8080}")
    private int port;

    @Autowired
    private OrderService orderService;

    @BeforeEach
    void setUp() {
        RestAssured.baseURI = "http://localhost:" + port;
        var order = Order.builder().tableNumber(1)
                .reservationStartTime(LocalDate.EPOCH.atStartOfDay())
                .reservationEndTime(LocalDate.EPOCH.atStartOfDay().plusHours(2))
                .status(OrderStatus.PENDING)
                .menuItems(List.of(MenuItem.builder()
                        .name("Burger")
                        .description("Cheese burger")
                        .price(BigDecimal.valueOf(13.6))
                        .type(ItemType.FOOD).build())).build();
        orderService.addOrder(order).subscribe();
    }

}
