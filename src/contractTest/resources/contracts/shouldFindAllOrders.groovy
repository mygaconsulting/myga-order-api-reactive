package contracts


import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method GET()
        url "/myga/api/v1/orders"
        headers {
            contentType applicationJson()
        }
    }

    response {
        status OK()
        body([[
                      tableNumber         : 1,
                      reservationStartTime: anyDateTime(),
                      reservationEndTime  : anyDateTime(),
                      status              : anyOf('PENDING', 'STARTERS_READY'),
                      menuItems           : [
                              [
                                      name       : 'Burger',
                                      type       : 'FOOD',
                                      description: 'Cheese burger',
                                      price      : 13.6,
                              ]
                      ]
              ]])
    }

}