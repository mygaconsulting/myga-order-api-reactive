package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method POST()
        url "/myga/api/v1/orders"
        headers {
            contentType applicationJson()
        }
        body(
                tableNumber: 1,
                reservationStartTime: anyDateTime(),
                reservationEndTime: anyDateTime(),
                status: anyOf('PENDING', 'STARTERS_READY'),
                billAmount: 0,
                menuItems: [
                        [
                                name       : anyAlphaNumeric(),
                                type       : 'FOOD',
                                description: anyAlphaNumeric(),
                                price      : anyDouble()
                        ]
                ]
        )
    }

    response {
        status CREATED()
        body(
                tableNumber: 1,
                reservationStartTime: anyDateTime(),
                reservationEndTime: anyDateTime(),
                status: anyOf('PENDING', 'STARTERS_READY'),
                menuItems: [
                        [
                                name       : anyAlphaNumeric(),
                                type       : 'FOOD',
                                description: anyAlphaNumeric(),
                                price      : anyDouble(),
                        ]
                ]
        )
    }

}
