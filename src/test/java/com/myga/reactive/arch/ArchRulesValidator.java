package com.myga.reactive.arch;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

@AnalyzeClasses(packages = "com.myga.reactive", importOptions = ImportOption.DoNotIncludeTests.class)
class ArchRulesValidator {

    @ArchTest
    static final ArchRule domain_should_not_call_infra =
        noClasses().that().resideInAPackage("..domain..").should()
            .dependOnClassesThat().resideInAPackage("..infra..");

    @ArchTest
    static final ArchRule domain_should_not_call_usecase =
        noClasses().that().resideInAPackage("..domain..").should()
            .dependOnClassesThat().resideInAPackage("..usecase..");

}
