package com.myga.reactive.usecase;

import com.myga.reactive.TestUtils;
import com.myga.reactive.domain.order.Order;
import com.myga.reactive.domain.order.OrderService;
import com.myga.reactive.infra.topic.OrderPublisher;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class OrderUseCaseTest {
    @Mock
    private OrderService orderService;
    @Mock
    private OrderPublisher orderPublisher;
    private OrderUseCase orderUseCase;

    @BeforeEach
    void setUp() {
        orderUseCase = new OrderUseCase(orderService, orderPublisher);
      }

    @Test
    void should_place_Order() {
        Order stubOrder = TestUtils.getStubOrder();
        Mockito.when(orderService.addOrder(stubOrder)).thenReturn(Mono.just(stubOrder));
        Mockito.doNothing().when(orderPublisher).publishOrder(any());

        Assertions.assertThat(stubOrder.getBillAmount()).isNull();

        StepVerifier.create(orderUseCase.placeOrder(stubOrder))
                .assertNext(order -> Assertions.assertThat(order.getBillAmount())
                        .isEqualByComparingTo(stubOrder.getMenuItems().get(0).getPrice())).verifyComplete();

        Mockito.verify(orderPublisher, times(1)).publishOrder(any());
    }
}