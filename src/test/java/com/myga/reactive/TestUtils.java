package com.myga.reactive;

import com.myga.reactive.domain.order.Order;
import com.myga.reactive.domain.order.OrderStatus;
import com.myga.reactive.domain.order.menuitem.ItemType;
import com.myga.reactive.domain.order.menuitem.MenuItem;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public final class TestUtils {
    private TestUtils(){

    }

    public static Order getStubOrder() {
        return Order.builder().tableNumber(1)
                .reservationStartTime(LocalDate.EPOCH.atStartOfDay())
                .reservationEndTime(LocalDate.EPOCH.atStartOfDay().plusHours(2))
                .status(OrderStatus.PENDING)
                .menuItems(List.of(MenuItem.builder()
                        .name("Burger")
                        .description("Cheese burger")
                        .price(BigDecimal.valueOf(13.6))
                        .type(ItemType.FOOD).build())).build();
    }
}
