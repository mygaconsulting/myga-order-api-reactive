package com.myga.reactive.domain.order;

import com.myga.reactive.TestUtils;
import com.myga.reactive.infra.repository.InMemoryOrderRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.test.StepVerifier;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    private final InMemoryOrderRepository repository = new InMemoryOrderRepository();
    private OrderService orderService;

    @BeforeEach
    void setUp() {
        orderService = new OrderService(repository);
    }

    @AfterEach
    void tearDown() {
      repository.clearAll();
    }

    @Test
    void should_not_found_order() {
        StepVerifier.create(orderService.getOrder(0, LocalDate.EPOCH.atStartOfDay()))
                .expectComplete()
                .verify();
    }

    @Test
    void should_found_placed_order() {
        Order order = TestUtils.getStubOrder();
        repository.save(order).subscribe();
        StepVerifier.create(orderService.getOrder(1, LocalDate.EPOCH.atStartOfDay()))
                .assertNext(persistedOrder -> assertThat(persistedOrder).isEqualToComparingFieldByField(order))
                .verifyComplete();
    }

    @Test
    void should_get_All_Orders_By_Priority() {
        var thirdOrder = TestUtils.getStubOrder().withTableNumber(4).withStatus(OrderStatus.PENDING);
        var firstOrder = TestUtils.getStubOrder().withTableNumber(2).withStatus(OrderStatus.PRINCIPAL_MEAL_SHIPPED);
        var secondOrder = TestUtils.getStubOrder().withTableNumber(3).withStatus(OrderStatus.PRINCIPAL_MEAL_READY);
        repository.save(secondOrder).subscribe();
        repository.save(firstOrder).subscribe();
        repository.save(thirdOrder).subscribe();
        StepVerifier.create(orderService.getAllOrdersByPriority())
                .expectNextSequence(List.of(firstOrder,secondOrder, thirdOrder))
                .expectComplete()
                .verify();
    }

    @Test
    void should_add_order() {
        Order stubOrder = TestUtils.getStubOrder();
        StepVerifier.create(orderService.addOrder(stubOrder))
                .assertNext(order -> assertThat(order).isEqualTo(stubOrder))
                .verifyComplete();
    }
}