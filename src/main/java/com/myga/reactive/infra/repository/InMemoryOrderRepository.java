package com.myga.reactive.infra.repository;

import com.myga.reactive.domain.order.Order;
import com.myga.reactive.domain.order.OrderRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
@Profile({"dev", "test"})
public class InMemoryOrderRepository implements OrderRepository {
    private final List<Order> inMemoryTable = new ArrayList<>();

    @Override
    public Mono<Order> findBy(Integer tableNumber, LocalDateTime reservationStartTime) {
        return Flux.fromIterable(inMemoryTable)
                .filter(order -> Objects.equals(order.getTableNumber(), tableNumber))
                .filter(order -> Objects.equals(order.getReservationStartTime(), reservationStartTime))
                .next();
    }

    @Override
    public Mono<Order> save(Order order) {
        inMemoryTable.add(order);
        return Mono.just(order);
    }

    @Override
    public Flux<Order> findAll() {
        return Flux.fromIterable(inMemoryTable);
    }

    @Override
    public Mono<Boolean> deleteOrder(Integer tableNumber, LocalDateTime reservationStartTime) {
        return Mono.just(inMemoryTable.removeIf(order -> Objects.equals(order.getTableNumber(),tableNumber) && Objects.equals(order.getReservationStartTime(),reservationStartTime)));
    }

    public void clearAll() {
        inMemoryTable.clear();
    }
}
