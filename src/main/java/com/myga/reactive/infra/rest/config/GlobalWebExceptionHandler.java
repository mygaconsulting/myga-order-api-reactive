package com.myga.reactive.infra.rest.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.io.Serializable;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
public class GlobalWebExceptionHandler {
    public static Mono<ServerResponse> handle(Throwable throwable, ServerRequest serverRequest) {
        log.error("Failed Request {}",serverRequest.uri());
        return Mono.error(throwable)
                .flatMap(e -> ServerResponse.status(INTERNAL_SERVER_ERROR).bodyValue(e))
                .onErrorResume(IllegalArgumentException.class, GlobalWebExceptionHandler::handleBadRequest)
                .onErrorResume(RuntimeException.class, GlobalWebExceptionHandler::defaultExceptionHandler);
    }
    private static Mono<ServerResponse> handleBadRequest(IllegalArgumentException ex) {
        var apiError = new ApiError(BAD_REQUEST.getReasonPhrase(), ex.getMessage());
        return ServerResponse.badRequest().bodyValue(apiError);
    }
    private static Mono<ServerResponse> defaultExceptionHandler(RuntimeException ex) {
        var apiError = new ApiError(INTERNAL_SERVER_ERROR.getReasonPhrase(), ex.getMessage());
        return ServerResponse.status(INTERNAL_SERVER_ERROR.value()).bodyValue(apiError);
    }
    public record ApiError(String reason, String message) implements Serializable {}

}
