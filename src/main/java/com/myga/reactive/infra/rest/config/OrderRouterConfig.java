package com.myga.reactive.infra.rest.config;

import com.myga.reactive.domain.order.Order;
import com.myga.reactive.infra.rest.OrderController;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.media.StringSchema;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.annotations.RouterOperation;
import org.springdoc.core.annotations.RouterOperations;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class OrderRouterConfig {

    public static final String MYGA_API_BASE_PATH = "/myga/api/v1";

    @Bean
    @RouterOperations(
            {
                    @RouterOperation(path = MYGA_API_BASE_PATH + "/orders",
                            produces = {MediaType.APPLICATION_JSON_VALUE},
                            method = RequestMethod.POST,
                            beanClass = OrderController.class,
                            beanMethod = "addOrder",
                            operation = @Operation(operationId = "addOrder", responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Order.class))), @ApiResponse(responseCode = "400", description = "Invalid Employee details supplied")}, requestBody = @RequestBody(content = @Content(schema = @Schema(implementation = Order.class))))),
                    @RouterOperation(path = MYGA_API_BASE_PATH + "/orders/{table-number}",
                            produces = {MediaType.APPLICATION_JSON_VALUE},
                            method = RequestMethod.GET,
                            beanClass = OrderController.class, beanMethod = "getOrder",
                            operation = @Operation(operationId = "getOrder", responses = {@ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = Order.class))), @ApiResponse(responseCode = "400", description = "Invalid table number supplied"), @ApiResponse(responseCode = "404", description = "Order not found")}, parameters = {@Parameter(in = ParameterIn.PATH, name = "table-number")})),
                    @RouterOperation(path = MYGA_API_BASE_PATH + "/orders",
                            produces = {MediaType.APPLICATION_JSON_VALUE},
                            beanClass = OrderController.class,
                            method = RequestMethod.GET,
                            beanMethod = "getAllOrders",
                            operation = @Operation(operationId = "getAllOrders", responses = {@ApiResponse(responseCode = "200", description = "Orders list", content = @Content(schema = @Schema(implementation = Order.class))), @ApiResponse(responseCode = "404", description = "No content")}
                            ))})
    public RouterFunction<ServerResponse> ordersRoutes(OrderController orderController) {
        String ordersPath = MYGA_API_BASE_PATH + "/orders";
        return RouterFunctions.route()
                .POST(ordersPath, orderController::addOrder)
                .GET(ordersPath+ "/{table-number}", orderController::getOrder)
                .GET(ordersPath, orderController::getAllOrders)
                .DELETE(ordersPath+"/{table-number}", orderController::deleteOrder)
                .onError(Throwable.class, GlobalWebExceptionHandler::handle)
                .build();
    }

    @Bean
    public GroupedOpenApi groupApi() {
        return GroupedOpenApi.builder()
                .group("orders")
                .pathsToMatch("/myga/api/v1/orders/**")
                .addOpenApiCustomiser(getOpenApiCustomiser())
                .build();
    }
    public OpenApiCustomiser getOpenApiCustomiser() {
        return openAPI -> openAPI.getPaths().values().stream().flatMap(pathItem ->
                        pathItem.readOperations().stream())
                .forEach(operation ->
                    operation.addParametersItem(new io.swagger.v3.oas.models.parameters.Parameter().name("locale").in("header").
                            schema(new StringSchema().example("FR")).required(false))
                );
    }

}
