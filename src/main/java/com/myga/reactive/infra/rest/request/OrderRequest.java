package com.myga.reactive.infra.rest.request;

import org.springframework.web.reactive.function.server.ServerRequest;

import java.time.LocalDateTime;

public record OrderRequest(Integer tableNumber, LocalDateTime reservationStartTime) {
    public static final String TABLE_NUMBER = "table-number";
    public static final String RESERVATION_START_TIME = "reservation-start-time";

    public static OrderRequest getInstance(ServerRequest serverRequest){
        Integer tableNumber = Integer.valueOf(serverRequest.pathVariable(TABLE_NUMBER));
        LocalDateTime reservationStartTime = LocalDateTime.parse(serverRequest.queryParam(RESERVATION_START_TIME).orElseThrow());
        return new OrderRequest(tableNumber, reservationStartTime);
    }

}
