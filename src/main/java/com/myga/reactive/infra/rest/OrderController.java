package com.myga.reactive.infra.rest;

import com.myga.reactive.domain.order.Order;
import com.myga.reactive.domain.order.OrderService;
import com.myga.reactive.infra.rest.request.OrderRequest;
import com.myga.reactive.usecase.OrderUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.server.ServerResponse.created;
import static org.springframework.web.reactive.function.server.ServerResponse.noContent;
import static org.springframework.web.reactive.function.server.ServerResponse.notFound;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Controller
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;
    private final OrderUseCase orderUseCase;

    public Mono<ServerResponse> addOrder(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(Order.class)
                .flatMap(orderUseCase::placeOrder)
                .flatMap(order -> created(serverRequest.uri()).bodyValue(order));
    }

    public Mono<ServerResponse> getOrder(ServerRequest serverRequest) {
        var orderRequest = OrderRequest.getInstance(serverRequest);
        return orderService.getOrder(orderRequest.tableNumber(), orderRequest.reservationStartTime())
                .flatMap(ok()::bodyValue)
                .switchIfEmpty(notFound().build());
    }

    public Mono<ServerResponse> getAllOrders(ServerRequest serverRequest) {
        return orderService.getAllOrdersByPriority()
                .collectList()
                .flatMap(ok()::bodyValue)
                .switchIfEmpty(noContent().build());
    }

    public Mono<ServerResponse> deleteOrder(ServerRequest serverRequest) {
        var orderRequest = OrderRequest.getInstance(serverRequest);
        return orderService.deleteOrder(orderRequest.tableNumber(), orderRequest.reservationStartTime())
                .filter(Boolean::booleanValue)
                .flatMap(ok()::bodyValue)
                .switchIfEmpty(notFound().build());
    }

}