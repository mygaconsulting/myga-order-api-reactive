package com.myga.reactive;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
@OpenAPIDefinition(info = @Info(title = "Myga reactive api", version = "1.0", description = "sample documents"
))
public class MygaReactiveSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MygaReactiveSampleApplication.class, args);
        log.info("**********************************************************************");
        log.info("Order sample api from Myga is UP");
        log.info("Health check is available at /actuator/health");
        log.info("Swagger is available at myga/webjars/swagger-ui.html");
        log.info("**********************************************************************");
    }

}
