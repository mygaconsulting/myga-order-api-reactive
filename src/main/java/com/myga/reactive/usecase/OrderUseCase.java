package com.myga.reactive.usecase;

import com.myga.reactive.domain.order.Order;
import com.myga.reactive.domain.order.OrderService;
import com.myga.reactive.infra.topic.OrderPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@Profile({"dev" , "test"})
public class OrderUseCase {
    private final OrderService orderService;
    private final OrderPublisher orderPublisher;

    public Mono<Order> placeOrder(Order order) {
        return Mono.just(order)
                .map(Order::calculateBillAmount)
                .flatMap(orderService::addOrder)
                .doOnSuccess(orderPublisher::publishOrder);
    }

}
