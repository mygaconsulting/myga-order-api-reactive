package com.myga.reactive.domain.order;

import com.myga.reactive.domain.order.menuitem.MenuItem;
import lombok.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Getter
@Builder
@Setter
@With
@EqualsAndHashCode
public class Order {
    private Integer tableNumber;
    private LocalDateTime reservationStartTime;
    private LocalDateTime reservationEndTime;
    private OrderStatus status;
    private BigDecimal billAmount;
    private List<MenuItem> menuItems;

    public Order calculateBillAmount() {
        this.billAmount = this.menuItems.stream()
                .map(MenuItem::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return this;
    }

    public int calculateOrderPriority() {
        return BigDecimal.valueOf(ChronoUnit.SECONDS.between(reservationStartTime, reservationEndTime))
                .divide(BigDecimal.valueOf(status.getMaxReservationWaitingTimePercent()), RoundingMode.HALF_DOWN)
                .multiply(BigDecimal.valueOf(100))
                .intValue();
    }

    @Override
    public String toString() {
        return "Order{" +
                "tableNumber=" + tableNumber +
                ", reservationStartTime=" + reservationStartTime +
                ", reservationEndTime=" + reservationEndTime +
                ", status=" + status +
                ", billAmount=" + billAmount +
                ", menuItems=" + menuItems +
                '}';
    }
}
