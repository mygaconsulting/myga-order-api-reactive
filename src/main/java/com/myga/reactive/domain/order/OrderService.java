package com.myga.reactive.domain.order;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.Comparator;

@Component
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public Mono<Order> getOrder(Integer tableNumber, LocalDateTime reservationStartTime) {
        return orderRepository.findBy(tableNumber, reservationStartTime);
    }

    public Flux<Order> getAllOrdersByPriority() {
        return orderRepository.findAll()
                .sort(Comparator.comparing(Order::calculateOrderPriority));
    }

    public Mono<Order> addOrder(Order order) {
        return orderRepository.save(order);
    }

    public Mono<Boolean> deleteOrder(Integer tableNumber, LocalDateTime reservationStartTime) {
        return orderRepository.deleteOrder(tableNumber, reservationStartTime);
    }

}
