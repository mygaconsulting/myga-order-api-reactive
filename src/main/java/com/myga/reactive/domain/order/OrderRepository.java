package com.myga.reactive.domain.order;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

public interface OrderRepository {
    Mono<Order> findBy(Integer tableNumber, LocalDateTime reservationStartTime);
    Mono<Order> save(Order order);
    Flux<Order> findAll();
    Mono<Boolean> deleteOrder(Integer tableNumber, LocalDateTime reservationStartTime);

}
