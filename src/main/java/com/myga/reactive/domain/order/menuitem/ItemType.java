package com.myga.reactive.domain.order.menuitem;

public enum ItemType {
    FOOD, DRINK, SOFT_DRINK
}
