package com.myga.reactive.domain.order;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OrderStatus {
    PENDING(1),
    STARTERS_READY(5),
    STARTERS_SHIPPED(10),
    PRINCIPAL_MEAL_READY(20),
    PRINCIPAL_MEAL_SHIPPED(30),
    DESSERT_READY(65),
    DESSERT_SHIPPED(80),
    CLOSED(100);

    private final int maxReservationWaitingTimePercent;
}
