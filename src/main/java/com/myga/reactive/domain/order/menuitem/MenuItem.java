package com.myga.reactive.domain.order.menuitem;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Builder
@Getter
@Setter
@EqualsAndHashCode
public class MenuItem {
    private String name;
    private ItemType type;
    private String description;
    private BigDecimal price;

    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
